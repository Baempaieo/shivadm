#### Table of contents
* <a href="#introduction">Introduction</a>;
    * <a href="#license">License</a>; 
    * <a href="#accessibility">Accessibility</a>;
    * <a href="#existence-of-the-documentation">Existence of the documentation</a>;
    * <a href="#author-notes">Author Notes</a>;
    
* <a href="#shiva-engine">ShiVa Engine</a>;
    * <a href="#quick-history">Quick history</a>;
    * <a href="#proprietary-formats">Proprietary formats</a>;
    * <a href="#libraries-and-tools">Libraries and Tools</a>;
        * <a href="#obtaining-a-copy-of-shiva-nowadays">Obtaining a copy of ShiVa nowadays</a>;

* <a href="#datamining">Datamining</a>;
    * <a href="#pre-requirements">Pre-requirements</a>;
        * <a href="#setting-up-termux">Setting up Termux</a>;
    * <a href="#tools">Tools</a>;
    * <a href="#obtaining-smf-stk-and-ste-archives">Obtaining `.smf`, `.stk`, and `.ste` archives</a>;
    * <a href="#structure-of-a-shiva-engine-game">Structure of a ShiVa Engine game</a>;
        * <a href="#more-proprietary-formats">(More) Proprietary formats!</a>;
    * <a href="#using-the-tools">Using the tools</a>;

* <a href="#debugging">Debugging</a>;
    * <a href="#on-android">On Android</a>;
    * <a href="#on-linux">On Linux</a>;

* <a href="#modding">Modding</a>;
    * <a href="#creating-unofficial-ports">Creating Unofficial ports</a>;
    * <a href="#changing-the-game-assets">Changing the Game Assets</a>;
        * <a href="#editing-audio">Editing Audio</a>;
        * <a href="#editing-textures">Editing Textures</a>;
        * <a href="#editing-scripts">Editing Scripts</a>;
    * <a href="#repacking-and-testing">Repacking and Testing</a>;
        * <a href="#distributing-your-mod">Distributing your Mod</a>;

* <a href="#epilogue">Epilogue</a>;
* <a href="#sources">Sources</a>;

---

# Introduction
Welcome to the **ShiVaDM** (***ShiVa******D****ata****M****ine*) documentation!  
This is a rich, highly exhaustive, and unofficial documentation (wrote by yours truly, *Baempaieo*) that should cover every nook and cranny on how to datamine, modify (or *mod*), and debug *any* game coming from this defunct, yet nichè, game engine: *ShiVa Engine*!

The following should have just as enough info for the reader to come out, at the end of everything, with enough knowledge about it:  
but, before venturing into such, here are some ***very important*** points that should be aknowledged...

## License
The following (unofficial) documentation is licensed under **CC BY-NC 4.0** (**Creative Commons Attribution-NonCommercial 4.0 International**)- what does that mean?
This means that the following user can freely redistribute & remix my work, but at the cost of *giving credit where is due* (since i've wrote all of this alone) and *don't use the following work for commercial purposes* (since i want to make all of this knowledge **free & accessible for/to everyone**).

Atleast, this is the whole license "in a nutshell". If you want to give a read to the full thing, you can either give it a look at my repository (see `LICENSE`) or [Creative Commons' webpage](https://creativecommons.org/licenses/by-nc/4.0).

## Accessibility
Due to the point stated earlier, this whole documentation is converted into an handful of formats (**plain text**, **HTML**, **OpenDocument**, and **EPub**), just to make it accessible to a fair amount of (new and old) devices-
of course, in pure open-source spirit, i've done all of this thanks to [Pandoc](https://pandoc.org)!

## Existence of the documentation
It all started around in 2019-20, as far as i can remember, and a much younger me was trying to get a model extracted from a game made in said game engine.

Since i really *didn't knew* a lot of what was i doing (datamining, file extensions, file headers, etc.) in-depth, all i've used to do was, at max, fiddling with said unknown file on a hex-editor on Android.  
Honestly, reason why i wanted to do so was purely as a *self-achievement*, all without disclosing all on the details as to how:  
all until, one day, i had saw a random guy (which i would keep it's username undisclosed) over Instagram.  

Apparently they found out how to do the same thing i wanted to do, plus including pictures of a random model opened into a 3D manipulation program....*but there was a* ***very evident*** *catch*: **clout**. Lots of it.  
They left the process "a secret" on purpose, so that, let's say, a lesser-technical-knowledgeable person, once got interested into the whole thing, got baited to pay for a cheap script (neither the language it was written with was disclosed, too) and perform the transaction via their PayPal account.

I really wasn't liking the whole thing, so, on a much-later moment, on Discord, i had gotten in touch with said person that did such a "stark" discovery on that platform.  
If i were to literally nutshell how the whole thing went (and add a veil of comedy to the already-serious response from them), it all boiled down to:  
> *"Just figure things out and use IDA Pro lmao"*  

and that's probably where, in all fairness, it was the most useless advice i had ever got in my life- that is, force myself to use proprietary software just to figure out a proprietary, undocumented, file format (shocker, i know: use the proprietary on the proprietary, wow!).

After that, i've went on a very, very long journey alone on how to figure the mystery behind ShiVa Engine's proprietary file formats:  
on and off, coming to do more discoveries and then feeling what was i doing was futile...sometimes *even* relapse what i've already done 1 or 2 years later, but with more knowledge on tech- but cutting things short, it took me **5 years** to figure the whole thing...and as of late April of this year (2024), all of this stubborness i had towards coming back to it has, eventually, led me understand how everything worked:  
(almost) inside-and-out of the game engine, all by using FOSS tools in the process too (even though it was all using `hexedit`, Wine, and an handful of other stuff). 

Sure, maybe i may have found out that *since 2019 i was looking at the wrong file format*, but i am glad to announce that, since i had figured things **even more** in 2023 (adding extra knowledge acquired in 2024):  
this is where i've got the idea to **share said knowledge to the rest of the world!**

No more gatekeeping and nothing to hide- *anymore!*

## Author Notes
* The information included into this document is compatible with every asset produced under any version of the engine (`1.6.2`, `1.9.2`, and older versions);  
* Some information may be subject to changes between a commit and another, as such is constantly updated to contain fully-informed, researched, and tested notions about the game engine- this getting rid of inaccuracies and/or wrong assumptions about some of it's components (and more);  
* Throughout the sections, i would be using a lot of `UNIX-like` terminal commands here and there. This is due to me using *Linux* as my daily-driver;
* The game that will be played around with throughout this documentation (as a way to offer practical examples, too) will be [**Dust: Offroad Racing**](https://catmoonproductions.com/en/content/dust-offroad-racing), a racing game made in ShiVa by *CATMOON Productions*;  

---

# ShiVa Engine
Moving on, here we are, straight to the game engine itself: **ShiVa Engine**.  
Into this section, it will be introduced stuff like:  
it's history, the proprietary file formats it brings along, libraries and tools (that it uses for producing it's proprietary assets), and how to obtain a copy of ShiVa in 2024!

## Quick history
**ShiVa Engine** was a cross-platform 3D, closed-source, game engine made by *ShiVa Technologies SaS* (formerly called *StoneTrip*, a game company based in France): 
originally started as [**Shiva Suite**](https://web.archive.org/20041128022558) back in 2004, it partook into numerous changes too, like [**StoneTrip Development Kit**](https://web.archive.org/20060418010258) (or **STDK**) in 2006, then reverted it's name back into [**ShiVa**](https://web.archive.org/20080103102432/http://stonetrip.com) (with the capitalized **V**) in 2008.  
From 2008 onwards, the engine itself hasn't took any drastical changes to speak of, honestly.

With this game engine you can pretty much do a wide variety of 3D videogames (for multiple platforms), program it's logics with **Lua** (it's scripting language of choice), with a simple WYSIWYG editor (all designed with a dark & purple UI, to match with it's logo).

ShiVa has stood for quite a long time around, despite the company has been slowly neglecting the engine until **March 2023**:  
that was the exact date when *ShiVa* (the company) has decided to [publically announce it's EOS](https://web.archive.org/web/20230325203534/https://shiva-engine.com). Since that, the company has wiped (almost) everything about the engine, this only leaving few things like it's YT Channel and Facebook page.

Despite everything, there's one tiny glimmer of hope to become FOSS, eventually- even though there's just one problem about it, and i quote:  
>> *"... Because there are still customers/companies relying on ShiVa being closed source. If this ever changed, we would certainly think about it."*  
>
> *- ShiVa Technologies SaS, 2023, as a response to a comment into their [latest Facebook post](https://www.facebook.com/shivaengine/posts/693483432471765) (website & engine post-deletion)*

Yes, this is the reason why this'll never happen (for the moment)- this is due to, obviously, an handful of companies not dropping the ball yet despite it's EOS.

## Proprietary formats
And since the engine's nature is closed-source: it won't be enough to introduce it's own proprietary file formats, too!  
Of course, this is just an appetizer for the whole *datamining* aspect of the game engine itself.  
As of `10-01-2025`, with enough tinkering on my hands, they *might* be all one archive format with slight changes here and there (like file signature & extension).

| **File Extension** | **MIME Type** | **Header (Hex)** | **Header (Unicode)** | **Description** |
|---|---|---|---|---|
| `.stk` | *ShiVa3D STK Document* | `06 08 00 00  00 20 20 20  20 20 20 20  20 xx` | `.....       ` | *A proprietary archive format. As seen from this [screenshot](https://cdn.tutsplus.com/cdn-cgi/image/width=594/mobile/uploads/legacy/Shiva3D-game-development/4/export1.png), these types of archives are referred as a `Runtime Package`. These types of files can be used both as a full game project, or supplementary DLCs to an already-existing game made in the engine. Such files can be renamed to anything the developer wishes.* |
| `.ste` | *ShiVa3D STE Document* | `04 08 00 00  00 20 20 20  20 20 20 20  20 xx` | `.....       ` | *(Another) proprietary archive format. According to the image linked previously, such is referred as an `Edition Archive`. There may be an hint that this was the go-to and predecessor of `.stk` (since they share the same file hierarchy post-extraction).* |
| `.smf` | *ShiVa3D SMF Document* | `05 08 00 00  00 20 20 20  20 20 20 20  20 xx` | `.....       ` | *(Yet another) proprietary archive format. Files with this file format are usually called in-full as `S3DMain.smf` only. Reason why this specific name is given is due to a library named `S3DClient.so` (related to `ShiVa3DClient`), and it's only task is to read and execute game packages with that name & extension only (else, the game won't boot at all). Such gets embedded when the developer feeds an `.stk` file through the Authoring tool & the target platform is a mobile one (like `Android`), which then, during compilation, the `.stk` file gets renamed to `S3DMain.smf` and it's file signature gets changed aswell. Curiously enough, despite this "complicated" process the Authoring tool undergoes, it shares the same compression algorithm and file structure as `.stk` and `.ste` projects do (meaning this can be, i guess, a format exclusive to mobile devices).* | 

## Libraries and Tools
And moving on, if you ever wish to install ShiVa on your machine (whether it'll be under Windows or running the executable under **Wine** on Linux):  
it surprisingly exposes almost every tool and library that it uses to compose it's own proprietary formats.

The following can be found at
```
C:\Program Files(x86)\ShiVa Technologies\ShiVa Editor Web\Tools
```
Below this very text, there's a pretty detailed output i had commented out in order to figure *which* tool & library does *what* inside the engine itself- of course, with the help of both looking around the internet & fiddling with the executables to figure everything out.

> **Author's Note:**  
> *The full folder path is excluded from the following output*

```
2010-07-31 Luac.exe              ---> Short for 'Lua Compiler', this tool compiles any .lua script into .lub. Runs version 5.0.3;
2012-01-18 PVRTexTool.exe        ---> Short for 'PowerVR Texture Tool', it's a tool made by Imagination Technologies for transcoding .jpg and .png files and output them as .tga, .tex, .dds, and .pvr textures. This is obviously the tool responsible for creating textures in a game;
2012-12-07 ShiVaTextureTool.exe  ---> A proprietary tool made by ShiVa. It converts a given texture into a compatible one for the engine;
2011-12-21 WMAEncode.exe         ---> WMA (Windows Media Audio) encoder;
2010-07-31 cudart32_30_14.dll    ---> An external library related to Nvidia's CUDA technology- this is one of the first components that ties-in with compatibility towards NVidia's GPUs;
2010-07-31 ffmpeg2theora.exe     ---> A fork ffmpeg that can transcode multiple media formats either into Ogg Theora (.ogv, video) or Ogg Vorbis (.ogg, audio);
2010-07-31 jpeg62.dll            ---> JPEG Image Library;
2010-07-31 lame.exe              ---> LAME (LAME Ain't an MP3 Encoder) executable for encoding different audio formats into .mp3 (even this one a FOSS program!)
2010-06-05 libeay32.dll          ---> Another library, this one's related to OpenSSL ("Open Secure Socket Layer");
2010-07-31 libpng12.dll          --.
                                    > Image format libraries: these being related to, respectively, PNG and TIFF encodings;
2010-07-31 libtiff3.dll          --'
2012-08-20 lua2cpp.exe           ---> This tool adds a 'bridge' between Lua and C++ in the engine, explained in few words;
2010-03-18 msvcp100.dll          --.
2010-07-31 msvcp71.dll             |
2010-07-31 msvcp80.dll             -> Lots of Windows-related libraries, may have a relation to ShiVa's proprietary GUI tools(?)
2010-06-17 msvcr100.dll            |  No clear idea if these are the responsible libraries for detecting whether it's tools are 
2010-07-31 msvcr71.dll             |  executed outside of the engine itself.
2010-07-31 msvcr80.dll           --'
2010-07-31 nvcompress.exe        --.
2010-07-31 nvdecompress.exe         > Nvidia-related tools: the first 2 being related to texture compression/decompression,
2007-11-27 nvdxt.exe               |  'nvdxt.exe' converts textures, and 'nvtt.dll' is the library used for the above tools; 
2010-07-31 nvtt.dll              --' 
2010-03-29 oggdec.exe            --. 
                                    > Ogg Vorbis encoder and decoder;
2010-07-31 oggenc2.exe           --'
2011-04-07 openssl.cfg           --.
2010-06-05 openssl.exe              > OpenSSL-related tools- these being, in order: a default config file, it's executable, and it's library;
2010-06-05 ssleay32.dll          --' 
2010-07-31 zlib1.dll             ---> A very important library, this is the compression lib that the engine uses for creating it's own .stk, .smf, and .ste archives!
```

### Obtaining a copy of ShiVa nowadays 
Since the engine (and it's website) went kaput back in 2023, it can't be obtained in other ways beyond using snapshots from WebArchive (still, big props to each and every volunteer that made the following resources available to the public!).

Aside that, the only distribution that got *entirely* salvaged was **Windows**- meanwhile *partially* for MacOS, since the only thing that got archived was `ShiVa Authoring Tool` (it can compile an already-made `.stk` file into an executable for one of the many platforms the engine supports). For the Linux users out there, as mentioned a couple of sections ago, the source tarballs never got archived in the first place:  
overall, though, any `UNIX-like` OS can execute the Windows installation package via **Wine**.

As a small disclaimer, the whole package is only capable of reading and exploring archives generated by itself, as actual *modding* is close-to-impossible in a legit way (as in using ShiVa as a whole) since many important/vital features are behind a paywall:  
but, on the bright side, such impositions can be bypassed by using a mixed assortment of FOSS and internal tools the engine uses-  
which it'll be covered much later into this documentation.  

| **Platform** | **stonetrip.com (Archived)** | **shiva-engine.com (Archived)** |
|---|---|---|
| ***Windows*** | [ShiVa 1.6.2 PLE](https://web.archive.org/web/20081111183549if_/http://www.stonetrip.com/content/ShiVa/ShiVa_1.6.2.PLE.exe) | [ShiVa 1.9.2.0 WEB](https://web.archive.org/web/20170609052217if_/http://www.shiva-engine.com/download/editor/release/192/ShiVa_1.9.2.0.WEB.exe), [ShiVa 1.9.2.0 BASIC](https://web.archive.org/web/20170422234847if_/http://www.shiva-engine.com/download/editor/release/192/ShiVa_1.9.2.0.BASIC.exe), [ShiVa 1.9.2.0 ADVANCED](https://web.archive.org/web/20170422234603if_/http://www.shiva-engine.com/download/editor/release/192/ShiVa_1.9.2.0.ADVANCED.exe)<sup>*needs activation key*</sup> |
| ***MacOS*** | [S3D Authoring Tool iPhone 1.6](https://web.archive.org/web/20081111184410if_/http://www.stonetrip.com/content/iPhone/S3DAuthoringTool-iPhone-1.6-MacOs.dmg) | [ShiVa Authoring Tool 1.4.0.0](https://web.archive.org/web/20161119175116if_/http://www.shiva-engine.com/download/editor/release/192/ShiVa%20Authoring%20Tool%201.4.0.0.dmg) |

---

# Datamining
And here we are, the juicier part of the documentation itself: ***datamining***!  
From this point onwards, the rabbit hole will go deeper over this topic, and so it will be exploring the data within a ShiVa-powered game-  
but before that, some preparation is needed firsthand:

## Pre-requirements
As anything that must be related to datamining, some stuff must be done firsthand before getting our hands to the tools that will be introduced into the next section.

To start everything in order, the whole can be done on **every OS** (this including *Android* aswell, but [Termux](https://termux.dev) must be installed firsthand. A tutorial on this will be introduced later after this section).
Aside, these are the pre-required tools for our job:  

* **A terminal emulator**, this is *mainly* important for Linux and Android, but such will be partially used on Windows- depending on *which* tool;
* **An hex editor**, any can suffice, even plugins made for **Notepad++** is enough. If you're on Linux, you can use a tool called `hexedit`- otherwise, if you're on Windows, you can even use tools like **HxD** for instance;
* **OpenJDK**, this is a ***must have*** tool that should be installed firsthand on our machine. For *vanilla builds*, hop over [OpenJDK.org](https://openjdk.org), find the **jdk.java.net/** hyperlink, download the archive for your machine, and follow the install instructions [here](https://openjdk.org/install/). If you're on Linux (or Termux), you can get the tool by using the distro's package manager's search feature (ex. `apt search openjdk-jre`). For *Microsoft builds*, you can refer to [Microsoft's OpenJDK Builds](https://learn.microsoft.com/en-us/java/openjdk/download) and get the installer for your OS and architecture;
* **Basic terminal knowledge**. There's nothing scary about, this requirement explains everything on it's own;
* *Lots, lots of patience!*

### Setting up Termux
> **Author's Note:**  
> *The following tutorial assumes that you already have installed [Termux-X11](https://github.com/termux/termux-x11) on your Android device.*  
> *If you're following the following on a device that runs Android 14+, you must install the `nightly` build of the app (else it won't work).*

After finding out that **termux-box** has been discontinued a long time ago, this section will introduce, instead, how to install [Hangover](https://github.com/AndreRH/hangover)!  
To start everything up, let's go into termux and type the following  
```
termux-setup-storage        ## This should setup storage permissions into Android
apt update; apt upgrade     ## Updates the termux repos

termux-change-repo          ## Only use this if you're having trouble with the command above
```
and with that, we have setup Termux itself and we're ready to roll!  

Now, it is time to setup Hangover- but don't worry, it's surprisingly painless to set it up as a whole  
```
apt install tur-repo x11-repo       ## This should install the 'Termux User Repository' and X11 repos to Termux
apt install hangover termux-x11     ## Here we'll install Hangover and Termux-X11 (if you've the nightly build installed, replace it with 'termux-x11-nightly')

apt install fluxbox aterm           ## And here we'll pick a WM and a terminal emulator. If you're intentioned to run a GUI program in Hangover, you must install these two
```

And after all of the setup is done:   
simply just open the `Termux-X11` app on your device's launcher, then get back into Termux and type this to start an X11 session  
```
termux-x11 :0 -xstartup "fluxbox"
```
and upon coming back to the `Termux-X11` app, you should see the window manager running!  
If you wish to open the terminal emulator, just do a right-click, then select `aterm`.
To close the session, simply just come back to Termux and do the `Ctrl-C` key-combo- that's it!

As an extra, if you want to execute a program via Hangover, just type into the terminal window (inside the X11 session)  
```
wine [program name]
```

And, well, with that out of the way:  
you can now execute *any Windows programs on your Android device* (with almost-native speed and lesser power consumption)!  
Though, by going along with the whole documentation, this should come in handy if you'd wish to run programs like **QuickBMS** and such. If you want to know how Hangover does all of it's "magic", i'd gladly invite you to give a quick look to their ***GitHub Repository*** for more.

## Tools
Moving on, here we are to the *actual* tools section of this documentation.  
All of the following tools were the ones i had found across my years-long journey in order to convert/unpack/decompress (almost) every asset from a ShiVa-powered game.

* [**QuickBMS**](https://aluigi.altervista.org/quickbms.htm);  
    * A specialized tool for unpacking and repacking peculiar archive formats from a vast selection of games and programs. This will be used for unpacking our fellow `.stk`, `.ste`, and `.smf` archives i had documented from earlier;
* [**PVRTexTool**](https://archive.org/details/powervrtools);
    * This tool should help up with converting (and decompressing) every `.pvr`, `.dds`, `.tex`, and `.etc` textures into `.png` and/or `.jpg`. I'd vividly suggest to only get the archive that contains *the Windows executables* inside, since all of the tarballs made for Linux distros contain dependencies for old and deprecated versions of numerous programs and libraries;  
* [**Unluac**](https://sourceforge.net/projects/unluac);
    * This tool will serve as *incredibly useful* for decompiling into `.lua` every single `.lub` file from the game. **Note:** this program requires a *Java Runtime installed* on your machine, since the latter has been written in Java (if you haven't installed `OpenJDK` yet on your machine, i'd suggest checking my <a href="#pre-requirements">**pre-requirements**</a> title and then come back into this section);  
* [**Hex2Obj**](https://archive.org/details/hex2obj_0.24c) or [**3D Model Researcher**](https://mr.game-viewer.org/download.php);
    * These 2 (manual) tools can convert from files that contain raw model data into actually readable `.obj` or `.dae` models. Take note, these tools require a certain ***advanced*** knowledge with hexadecimal offsets and such to know where you're going. If you feel **Hex2Obj** is too hard to learn from, you can give a read to **3D Model Researcher**'s [online guide](https://mr.game-viewer/tutorial.php)<sup>[archived snapshot](https://web.archive.org/web/20231012094752)</sup> for a more guided tutorial. Both were made to run *exclusively* on Windows, but they can work flawlessly *under Wine aswell*;

The rest of the tools below are just an optional, but, depending on the game or scenario you're into:  
these can either be useful for faster research or if you want to *just view* the mesh models.

* **Basic Linux Utils**. This extends to using tools like `grep`, `less`, `more`, pipes (`|`), and *some* basic knowledge of **bash scripting**;
* [**i3DConverter**](http://3doc.i3dconverter.com/index.html);
    * An universal, closed-source and cross-platform, 3D viewer and converter for numerous file formats. Only con is that, according to this [spreadsheet](http://3doc.i3dconverter.com/formats.html), any mesh (or `.msh`) file coming from any ShiVa game can only by loaded and viewed, but not converted and saved at all- to make things worse, they *must* come only from **Dust: Offroad Racing** (the game we'll be looking upon in this documentation);

## Obtaining `.smf`, `.stk`, and `.ste` archives 
> **Author's Note:**  
> *This section assumes that you've already prepared (and unpacked) QuickBMS and downloaded the game's Android package inside the `Download` folder of your machine, but also assumes that you already have an archive manager installed out-of-the-box.*  
> *Before proceeding, make sure you've both moved inside your machine's `Download` folder (either with the terminal or your file manager of choice) and gather the following by either consulting this documentation or the internet.*

As mentioned from the <a href="#proprietary-formats">**proprietary formats**</a> section:  
**ShiVa Engine** introduces over 3 new proprietary archive formats, and into <a href="#libraries-&-tools">**libraries & tools**</a> have also discovered the compression algorithm/library it refers to for creating them.

Alright, with that out of the way, it's time to cover on *how to obtain* such from an `.apk` file, how to use **QuickBMS**, and document along the way the extra proprietary formats it will introduce post-extraction.

Starting off, i have downloaded the `.apk` file for the game we'll be inspecting upon (again, **Dust: Offroad Racing**), which i had conveniently renamed as `Dust.apk` for the sake of this guide.  
Moving on, let's move inside the `Downloads` folder of our machine (this is where typically our downloaded files are at):  
since i use a terminal for my workflow, i simply move inside a folder by going  
```
cd Downloads
```
else, if you're using a graphical file manager (like *Explorer*, for instance), you can simply double-click on the folder.  

Going along, it is now our time to check inside the downloaded game!  
To check inside of it, we may need an *archive manager*, since `.apk` files are actually a `.zip` archive. Whether the archive manager may work graphically or via the terminal, what matters is that you can use it without issue yourself.  
Since i use the CLI version of `7z`, this is how i tend to check the contents of an archive  
```
7z l Dust.apk
```
and upon the output of the command, this is what we should find (hierarchically) inside the game
```
   Date      Time    Attr         Size   Compressed  Name
------------------- ----- ------------ ------------  ------------------------
2012-09-18 18:49:20 .....         1629          698  META-INF/MANIFEST.MF
2012-09-18 18:49:20 .....         1750          828  META-INF/MYKEY.SF
2012-09-18 18:49:20 .....         1383         1112  META-INF/MYKEY.RSA
2012-09-10 19:12:38 .....     44315768     44315768  assets/S3DMain.smf
2012-09-18 18:03:58 .....         6258         6258  res/drawable/app_icon.png
2012-07-18 14:46:04 .....       129138       129138  res/drawable/app_splash.png
2012-09-18 18:10:12 .....          564          291  res/layout/billing_not_supported.xml
2012-09-18 18:10:12 .....          640          322  res/layout/main.xml
2012-09-18 18:10:12 .....         5244         1521  AndroidManifest.xml
2012-09-18 18:10:12 .....         2428         2428  resources.arsc
2012-09-18 18:03:58 .....        12818        12818  res/drawable-hdpi/app_icon.png
2012-09-18 18:03:58 .....         6258         6258  res/drawable-ldpi/app_icon.png
2012-09-18 18:03:58 .....        12818        12818  res/drawable-mdpi/app_icon.png
2012-09-18 18:03:58 .....        12818        12818  res/drawable-xhdpi/app_icon.png
2012-09-18 18:10:10 .....       277188       121015  classes.dex
2012-09-18 18:09:54 .....       916168       463984  lib/armeabi/libcrypto.so
2012-09-18 18:09:52 .....       170875        75109  lib/armeabi/libopenal.so
2012-09-18 18:09:38 .....      8697384      2764151  lib/armeabi/libS3DClient.so
2012-09-18 18:09:52 .....       204072       107287  lib/armeabi/libssl.so
2012-09-18 18:09:54 .....       891568       456160  lib/armeabi-v7a/libcrypto.so
2012-09-18 18:09:54 .....       151511        69239  lib/armeabi-v7a/libopenal.so
2012-09-18 18:09:52 .....      8250484      2611400  lib/armeabi-v7a/libS3DClient.so
2012-09-18 18:09:54 .....       197308       104331  lib/armeabi-v7a/libssl.so
------------------- ----- ------------ ------------  ------------------------
2012-09-18 18:49:20           64266072     51275752  23 files
```
As we can see here, the file we need to extract (`S3DMain.smf`) can be found inside the `/assets/` folder- great!

To note more, aside the `.smf` file, we can also verify that a game is made in ShiVa even by inspecting further what there is inside the `/lib/armeabi*/` folder aswell- but why am i mentioning this?  
Remember when we have looked upon the ***libraries & tools*** section from much earlier?  
Alright, whenever the game engine packages the *whole* game for Android, several libraries get actually pulled inside the `/lib/armeabi*/` folder path. Each library exactly corresponds to a library used into the engine itself, like:  
* `libS3DClient.so` is the one responsible for reading and executing the `S3DMain.smf` file (inside the `/assets/` folder); 
* `libssl.so` establishes secure socket connections when communicating to a server; 
    * `libcrypto.so` is the library that, altogether with `libssl.so`, adds encryption when your device and the game's servers communicate with eachother- so that no external attacker may compromise any online traffic going on;  
* `libopenal.so` adds support for reading and playing all of the `.ogg` files inside the `S3DMain.smf` archive (which, apparently, the `opeanal` in `libopenal` stands for ***Open******A****udio****L****ibrary*- and which audio encoding is free and open-source? *Ogg Vorbis*!);  

Moving on, it is now time to extract the `.smf` archive from the game.  
Again, since i use a terminal, this is how i tend to extract one specific content from any archive
```
7z x Dust.apk assets/S3DMain.smf
```
and after extracted the latter, not only it would be outputted inside a folder named `assets`, but it also our time to shine with **QuickBMS**!

If you had prepared the tool already, i'd suggest creating 2 folders in particular:  
one for containing all of the extracted files from the `quickbms.zip` archive (and keep all of our `.bms` scripts inside), another where we will output the contents of the `S3DMain.smf` file at.  
Since, for the 1000th time, i use the terminal, i'll create the following  
```
mkdir ~/Downloads/quickbms      ## this'll be the folder for, obviously, QuickBMS
mkdir ~/Downloads/test          ## meanwhile this'll be the folder for containing the .smf file's contents
```

Now, time to use the tool- but **halt**, using QuickBMS actually differs between *Windows and Linux*:  
but both have in common the habit of moving firsthand to the folder where the executable is at- which, in this case, it should be  
```
cd ./quickbms
```
and then execute the tool like this
```
quickbms -G     ## this one's for Windows, it should execute the tool in GUI mode- which is the easiest way to use the tool 

chmod +x ./quickbms     ## and this section is for Linux. This one makes the tool executable...
./quickbms ~/Downloads/quickbms/shiva.bms ~/Downloads/assets/S3DMain.smf ~/Downloads/test/    ## ...and this one does the whole extraction manually
```

When the tool is done extracting the contents from the archive, you can either close the window or press any key to close the tool:  
but aside that, we have officially learned on how to find, extract, and unpack `S3DMain.smf`!

After all of this, though...what about `.stk` archives?  
`.stk` files in the wild are sort of hard to find, depending on what you're looking after, but you can mostly find several of this by exploring ShiVa Engine's internal files. These typically can be found inside the `C:\Program Files (x86)\ShiVa Technologies\ShiVa Editor Web\` folder path:  
even though the process boils down to the one i had described earlier, honestly- but they can be used greatly to practice more on how to use QuickBMS in general!

...and `.ste` archives?  
You would be in real surprise how *easy* they are to obtain, even after ShiVa has died for some time already!  
If you give a deep dive towards [ShiVa Technologies' official GitHub page](https://github.com/ShiVaTechOfficial?tab=repositories), you may find a great handful of repositories that contain this peculiar type of archive format.

## Structure of a ShiVa Engine game
After successfully learned together on how to extract a `.smf` archive, we can now finally look upon the general structure of a ShiVa Engine project/game as a whole.  

By cutting things short, the output below will have comments that explain, atleast to a fundamental degree, which file is responsible for what it does inside the game itself (and, as stated previously, explain, describe, and document each and every proprietary format after this very section)  
```
Games
|______  Dust.gam                   ## This folder portion contains the "core" that ShiVa reads to start the game;

Models
|______  Model_Core_Camera_Back.mdo ## This one contains model data, but not mesh data
         ...

Resources                           ## Any ShiVa game has this particular folder. 
|                                   ## This one nests the rest of the assets of the game itself, which, in-engine, get called externally by the files contained
|                                   ## within 'Games', 'Models', and 'Scenes'
|______  AIModels
|           |______  AI_Core_Main_CarConfigurator.aim       ## Not to be confused with *actual* game models- this one sets the behavior of a specific thing
|                    ...                                    ## if an action happens in-game.
|______  Fonts
|           |______  DefaultFont.fnt                        ## A simple folder containing fonts, but aren't .otf or .ttf files in disguise
|                    ...
|______  HUD
|           |______  HUD_Core_Main_CarConfig.hud            ## HUD data
|                    ...
|______  Materials
|           |_____   Collider.mat                           ## Material files, probably is connected to game textures
|                    ...
|______  Meshes
|           |_____   Car_1_lod1_Polygonal_Geometry_lib.msh  ## Mesh data 
|                    ...
|______  Musics
|           |_____   Music_Core_InGame_1.mus                ## Music data
|                    ...                                    
|______  Particles
|           |_____   Particle_CheckpointGate.par            ## Similar to the 'Materials' folder, that's all
|                    ...
|______  Scripts
|           |_____   AI_Core_Main_CarConfigurator_Function_GetAIVar.lub     ## This is where the scripting magic happens, 
|                    ...                                                    ## the format is derived from optimizing the original .lua scripts
|______  SoundBanks
|           |_____   Soundbank_Core_Cars.snb                ## This particular folder's task is to compile the audio data from the 'Sounds' folder into a
|                    ...                                    ## a Sound Bank, which may be used for particular assets of the game (like models, or actions)
|______  Sounds
|           |_____   Sound_Core_ButtonClick.snd             ## Sound data
|                    ...
|______  Textures
|           |_____   DefaultFont.dds                        ## Texture data
|                    ...
|______  Trails
            |_____   Trail_Core_WheelBurn.tra               ## Trail data, also may have a connection with game textures

Scenes
|______  Scene_Core_Dummy.scn                               ## Scene data. This one gives instructions how animations, models, and shaders must play to
         ...                                                ## compose an in-game scene
```

After given a pretty good look at how this ShiVa game is structured, we would now proceed moving over to the next title:  
as it will uncover what these files are *really* about and what they actually contain.

### (More) Proprietary formats!
As mentioned briefly at the end of the previous title, this section will purely document the technical info about the brand new proprietary file formats the engine brings (once again):  
of course, by introducing a simple table that gives a "general overview" *outside of the game engine's perspective*.   

| **File Extension** | **Header (Hex)** | **Header (Unicode)** | **Folder Path** | **Description** |
|---|---|---|---|---|
| `.gam` | `04 00 00 00  4E 63 50 xx` | `...NcP` | `/Games/` | *This specific file calls in a specific order every asset (in plain text) to execute the game itself thru the game engine: which can be textures, sounds, scripts, etc.* |
| `.mdo` | `03 00 00 00  4E 63 50 xx` | `...NcP` | `/Models/` | *These files are supposed to call the models inside the `/Resources/Models/` folder in a specific order (again, in plain text), alongside other misc details like skeleton data (if it is present within the game assets)* |
| `.scn` | `03 00 00 00  4E 63 49 xx` | `...NcI` | `/Scenes/` | *Just like the `.gam` file, even these sort of files get to call various assets around (once again, in plain text): only exception is that they get used to compose a game scene* |
| `.aim` | `03 00 00 00  4E 63 50 xx` | `...NcP` | `/Resources/AIModels/` | *These files are supposed to give "what if X happens" conditions to, let's say, CPU and other elements that are contained within the game itself. There may be a chance these communicate to the internal scripts of a project/game* |
| `.fnt` | `03 00 00 00  4E 63 58 xx` | `...NcX` | `/Resources/Fonts/` | *Haven't found anything interesting about these files, but they could be plain .ttf or .otf files converted in a certain way to work with the engine. Some fonts may also communicate with font spritesheets contained within the `/Resources/Textures/` folder path* |
| `.hud` | `03 00 00 00  4E 63 50 xx` | `...NcP` | `/Resources/HUD/` | *In a few words, these files call (again* ***again****,in plain text) specific textures, scripts, and sounds coming from the archive to compose an UI element in-game* |
| `.mat` | `03 00 00 00  4E 63 58 xx` | `...NcX` | `/Resources/Materials/` | *These type of files call, simply, (oh boy, in plain text, who could have predicted it!) specific textures contained into `/Resources/Textures/` to visualize materials in a model* |
| `.msh` | `03 00 00 00  4E 63 50 xx` | `...NcP` | `/Resources/Meshes/` | *Unlike most files around here, these files contain raw model metadata: this including vertex, polygon, and (possibly, if present) skeleton data* |
| `.mus` | `4F 67 67 53` | `OggS` | `/Resources/Musics/` | *Simply put, just an Ogg Vorbis file with a renamed extension* |
| `.par` | `03 00 00 00  4E 63 58 xx` | `...NcX` | `/Resources/Particles/` | *Yet again another series of files that call, well, in plain-text (wow!), assets within the `/Resources/Textures` folder path* |
| `.lub` | `1B 4C 75 61  50 xx xx xx` | `.LuaP` | `/Resources/Scripts/` | *A simple`.lua` script compiled with the `Luac` utility. These can be easily decompiled with external tools. According to [it's official manpage](https://www.lua.org/manual/5.0/luac.html), such compilation is done not only for obfuscation, but also for optimization of said script/s.* |
| `.snb` | `03 00 00 00  4E 63 58 xx` | `...NcX` | `/Resources/SoundBanks/` | *These files "containerize" (i'll let you guess in which format) a collection of "Sound Banks" by writing down the filenames coming from `/Resources/Sounds/` for specific elements of a game: whether it'll be a map, character, or anything else* |
| `.snd` | `4F 67 67 53` | `OggS` | `/Resources/Sounds/` | `Same as .mus` |
| `.dds` | `44 44 53 20` | `DDS` | `/Resources/Textures/` | *Texture data. This format is natively supported on Windows due to being MS' proprietary image format* |
| `.pvr` | `34 00 00 00  00 02 00 00  00 02 00 00  09 00 00 00  19 83 01 00  00 AB 02 00  04 00 00 00  00 00 00 00  00 00 00 00  00 00 00 00  01 00 00 00  50 56 52 21` | `4..........................................PVR!` | `/Resources/Textures/` | *Texture data. This format is Imagination's proprietary "PowerVR" texture format, optimized for the **PowerVR** architecture, which is greatly documented over [this pdf document](http://powervr-graphics.github.io/WebGL_SDK/WebGL_SDK/Documentation/Specifications/PVR%20File%20Format.Specification.pdf).* |
| `.etc` |  `34 00 00 00  00 02 00 00  00 02 00 00  09 00 00 00  36 01 01 00  B8 AA 02 00  04 00 00 00  FF FF FF FF  FF FF FF FF  FF FF FF FF  00 00 00 00  50 56 52 21` | `4...............6..........................PVR!` | `/Resources/Textures/` | `Same as .pvr` |
| `.tra` | `03 00 00 00  4E 63 58 xx` | `...NcX` | `/Resources/Trails/` | *Once again, yet another file that calls texture assets from `/Resources/Textures/`* | 

## Using the tools
Since 2 sections ago we have looked on how to extract the files form `S3DMain.smf`, we will now come to the point of *converting* most of these proprietary assets into actually usable files- and this is where the tools i've listed earlier come into play!  
Everything with the exception of the 3D-related tools, since, for the time being, the mesh data is unknown territory.

Keep in mind, the remaining tools that i will document it's usage below *must be used on a terminal*:
so, get ready to open your terminal emulator, move the needed tool to it's intended path (`/Resources/Textures/` if you're going to use **PVRTexTool**, or/and `/Resources/Scripts/` if you're going to use **unluac**), and get ready for some action!

#### PVRTexTool
##### Usage
```
PVRTexToolCLI -i Tex_Core_CheckpointGate_Star.pvr -o Tex_Core_CheckpointGate_Star.png
```
If you want to use the tool with more specific arguments, feel free to check Imagination's [documentation on the tool](https://docs.imgtec.com/tools-manuals/pvrtextool-manual/html/topics/cli/command-line-options.html) to know more.

#### Unluac
##### Usage
```
java -jar unluac AI_Shell_Main_Init_Function_HUD_Garage_ShowBuyCar.lub -o AI_Shell_Main_Init_Function_HUD_Garage_ShowBuyCar.lua
```

And as a last thing, i'll also introduce a *really* useful trick that should mass convert any file with some coding trickery.  
Yeah, i know this is a `for` loop in bash, but you can use it with any tool you may like for any of your mass-conversion needs- the text below is just some syntax boilerplate to get the point across  
```
for file in *.[input file extension]; do [tool] [input flag] "$file" [output flag] "$file".[output file extension]; done
```
Though, if you want to have *more* examples to digest the technique better:  
here's more, but with actual CLI tools used in the mix  
```
for file in *.txt; do mv "$file" "$file".bak; done

for file in *.lub; do java -jar unluac "$file" -o "$file".lua; done
```

If you're following all of this with a Windows machine (as of this latest update, written on `20/12/2024`), it looks like that *Batch* (the shell language used on Windows) isn't as flexible as Bash is in terms of `for` loops:  
so, as a way to recover from this lack of flexibility, i'd heavily suggest to get [Chocolatey](https://chocolatey.org/install) on your machine and install [Git](https://community.chocolatey.org/packages/git) with it.  

Why am i suggesting such is due to **Git** (for Windows) having a pretty particular feature, *GIT Bash*, which it can allow to execute a separate Bash shell that can be launched via the right-click/contextual menu inside the file explorer.

---

# Debugging
> ***Author's Note:***  
> *This section assumes that you either have a Linux machine with `ADB` and `Wine` installed, or, depending on how you're following this section, your Android device with a root manager and permissions allowed in it's ROM.*  
> *Eitherway, it also assumes that you have a Terminal Emulator installed aswell in order to proceed*.  
>  
> *If you're wondering why Windows has been excluded as a viable method on how to debug your favorite ShiVa game:*  
> *the fault falls more towards `StoneTrip`/`ShiVa Technologies SAS` than myself, since they had compiled `S3DEngine` for Linux with verbose output enabled by-default, but not for Windows. Sadly this thing is out of my control, so i can't do anything about it.*  

And here we go, another valuable piece to our documentation: ***debugging***!  
In this section, we'll explore on how to debug our ShiVa-powered game via *Linux* and *Android*:  
of course, just like most of this documentation, this'll require an heavy usage of the terminal (on both platforms).

Since i had never saw any information or official guide that discloses such process, i will share these valuable nuggets of undocumented debugging right here into my documentation. Get ready!

## On Android
If you're eager to debug a ShiVa game via Android, it is actually surprisingly doable in some capacity.  

Since the game's logfile is kept within the `/data/data/` folder path inside the device's ROM:  
you **must** have root permissions in order to inspect such, since Android is locked down by-default.  
Mind you, this portion *only* works if you have already booted up the game once, else it'll serve no purpose at all.

To start everything in order, if you have a PC in order to inspect inside your Android device (of course, already plugged-in, with **Developer Options** and **USB Debugging** enabled. If you don't know how to do such, give a read on [this official guide](https://developer.android.com/studio/debug/dev-options)), you must start the `adb` daemon firsthand with this command
```
adb start
```

Afterwards, the rest of the process follows up with these commands (again, commented for the sake of explaining what's going on)  
```
adb root    # This should start the adb daemon with root priviledges enabled 

su          # If you've a root manager in your device (ex. Magisk, KernelSU), you must do this firsthand

cd /data/data/com.catmoonproductions.dust/      # Let's get inside the game's data folder- this being the game we're inspecting upon, Dust
find . -name *.log          # If we don't know where the logfile is at, let's use the 'find' command
cd [path/to/S3DMain.log]    # Once got the folder path from 'find', copy-by-hand the results alongside 'cd'- make sure to exclude 'S3DMain.log' while writing!
less ./S3DMain.log          # This should open up 'less' to view our logfile. If you don't know it's keybinds, type 'h' to know more about it

cp S3DMain.log /sdcard/     # If you'd like to examine the logs via external programs, this should save a copy of it into it's internal memory
exit                        # Type this twice if you've used 'su' to get into the root session via the terminal
```
and with that done, you can rinse-and-repeat the process as much as you like, whenever you want to troubleshoot any issue/s on your favorite game (or want to understand why such may crash so often. If you aren't satisfied with this portion, you can still get your way at using `logcat` for a deeper analysis on the game).

## On Linux
Unlike the Android counterpart, the Linux side is quite involved a little bit:  
yet it doesn't require any root priviledges in the first place, but it involves more the usage of one of the internal tools included within **ShiVa Authoring Tool** than the whole thing- which i'll introduce it after setting up the latter.

In order to install the previously mentioned tool, we must refer all the way back to <a href="#obtaining-a-copy-of-shiva-nowadays">**obtaining a copy of ShiVa nowadays**</a> from several sections ago. What we really need is just the `WEB` variant of the engine downloaded on our machine.

Once downloaded said executable, we must get to our `Downloads` folder
```
cd Downloads
```
and then extract the **Authoring Tool** installer with any archive manager of our choice-  
which, in my case, again, i'll be using `7z` for the job
```
7z x ShiVa_1.9.2.0.WEB.exe shiva_authoring_tool_1.4.0.0.exe
```
and when that's done, lets remove the `WEB` installer and proceed installing the tool via `Wine`  
```
rm *WEB*
wine shiva_authoring_tool_1.4.0.0.exe
```
and with that, we should be at the installation wizard. Just follow along with it, since it's pretty easy to go through.  

Now though, you may be asking, *what* is the tool that we may need for debugging our picked game on Linux?  
the answer may sound silly, but the **Authoring Tool** has inside a tool named `S3DEngine` (short for `ShiVa3D Engine`).  
The following is capable of reading and executing `.ste`, `.stk`, and `.smf` packages, but beyond that, not only we can debug our favorite ShiVa-powered game, but we can also execute it with custom arguments aswell, such as:  
* A custom "environment" XML document hosted on a server (`-c=[PATH]`/`--server=[PATH]`);
* Custom window height and width (`-w=NUM`/`--width=NUM` and `-h=NUM`/`--height=NUM`);
* Fullscreen mode (`-f=0-1`/`--fullscreen=0-1`. `0` forces windowed mode, while `1` enables fullscreen (default option));
* Failsafe mode (`-F=0-1`/`--forcefailsafe=0-1`);
* Custom anti-aliasing (`a=0-3`/`--appaalevel=0-3`. `0` sets the antialias as defined into the engine's config file, `1` disables antialiasing, `2` sets antialiasing to `2x`, while `3` sets it to `4x`);
* In-Game Shadow levels (`-s=0-1`/`--appshadowlevel=0-1`);
* Post-rendering effects (`-p=0-1`/`--apppostrelevel=0-1`);
* Reflective surfaces (`-r=0-1`/`--appreflectlevel=0-1`);

With the arguments already introduced, let's get straight into the meat-and-potatoes of this portion and show how to use the tool.  
To find `S3DEngine`, we must go to the following folder path
```
cd .wine/drive_c/'Program Files (x86)'/'ShiVa Technologies'/'ShiVa Authoring Tool'/Data/Windows/Linux/Build
```
and, since we'll be presented by many variants (`headless`, `32-bit`, and `64-bit`) of it, just write in your terminal  
```
./S3DEngine_Linux_x86_64.bin --help
```
and you'll be welcomed by an `help` menu on how to use the tool.  

Now, where could we find in-handy an `.smf` or `.stk` file to test out the waters?  
Of course, we can refer to our previous attempt on how <a href="#obtaining-smf-stk-and-ste-archives">**to obtain one**</a> and run it with the tool we've found out right now!  

Since our `.smf` file is placed inside `~/Downloads/assets/`, we can give it to the tool like this  
```
./S3DEngine_Linux_x86_64.bin ~/Downloads/assets/S3DMain.smf
```
and upon executing this command, we'll be greeted not only with the game executing on our PC:  
but also a wonderful verbose output into the terminal, so that we can thoroughly debug for any issues, errors, or performance with the whole game *in real time*!  

Curiosly enough, this method works *even* for playing natively the game on your PC- and, yes, even the `Windows` port is capable of doing it too!  
If you're curious on where to find it, you can find it at
```
C:\Program Files (x86)\ShiVa Technologies\ShiVa Authoring Tool\Data\Windows\Windows\Build\S3DEngine_Windows.exe
```
though, with this out of the way: ***happy debugging!***

---

# Modding
Last but not least, here we are on the latest section of this whole documentation: ***modding***!  

If you may have followed all of the sections much before this one, i had actually teased a way on how to modify (or *mod*) a game made with ShiVa, but also previously introduced a tool named **ShiVa Authoring Tool** aswell.  
Now, here's the good news, this particular program can actually allow us to compile our `.stk`/`.smf` files into a functioning game, meaning that we can *create unofficial ports of our selected ShiVa-powered game!*

To start things off, though, we may need the tools for doing all of this:  
which it all boils down to having at-hand the tools <a href="#tools">**we had introduced into the "Datamining" section**</a> (even though i'd suggest just keeping **QuickBMS**, **Unluac**, **PVRTexTool**, and your favorite text editor), and the ones that were introduced <a href="#libraries-and-tools">**while we were looking together at the engine's internal tools and libraries**</a> (still, even this just boils down to just having **Luac**, **FFmpeg**, and, *once again*, **PVRTexTool**).

All of these tools are a *must* to have in order to:  
convert, edit, convert back, and re-importing our edited assets (plus passing the edited package to the *Authoring Tool* to compile it).

## Creating Unofficial ports
> ***Author's Note:***  
> *This method won't work with every game made in the engine!*  
> *Some developers like to design their own game to work exclusively with a particular CPU architecture:*  
> *but despite that, with the Authoring Tool anyone can cross-compile their selected game to another type of architecture- even if unofficially, such is doable by picking the same target platform the devs have picked, edit one setting into the `Build` tab, and let the Authoring Tool do it's job.*  
>  
> *For simplicity's sake, i'll show on how to compile the game for Windows only:*  
> *since, more or less, compiling for other platforms boils down to the same process.*  
>  
> *In special cases, though, compiling for `iPad` and `iPhone` are only exclusive to the MacOS edition of the Authoring Tool (which you can get it <a href="#obtaining-a-copy-of-shiva-nowadays">here</a>):*  
> *and other platforms (like `Android`, `PalmOS`, `Blackberry`, and `Wii`) require either specialized SDKs or miscellaneous software for compiling the game, hence why i'm skipping and simplifying things up.*

Since generally porting a game to another platform is, typically, pretty difficult and time consuming:  
this is where ShiVa's "multi-platform" nature starts to shine, allowing every type of person to port their favorite game with extreme ease!

The very first thing we must do, if we're intentioned to feed to the tool an `.smf` file to compile up, we must do these 2 commands firsthand
```
cd ~/Downloads/assets/      # This is where our previously extracted 'S3DMain.smf' file is at
mv S3DMain.smf Dust.stk     # And we'll be renaming it into 'Dust.stk', both for ease to find it & for the Authoring tool to compile it without issues
```
and when done so, we must create a copy of the file into an USB (must be `8GB` or higher in storage size):  
so that, when we pick it from the file explorer, it'll be easier and quicker to find.

After that, time to open **ShiVa Authoring Tool**.  
If you're using Linux, you must type the following in the terminal  
```
wine .wine/drive_c/'Program Files (x86)'/'ShiVa Technologies'/'ShiVa Authoring Tool'/'ShiVa Authoring Tool.exe'
```
and the tool will open in a snap!

To give a very quick guide on the UI:  
* On the left you'll find a vast selection of platforms (like `Windows`, `Linux`, etc.) to port the game into;
* Meanwhile, on the right, there are 2 big buttons going on;
    * The one on the left guides you on how to use the tool, meanwhile the one on the right gets straight into the compilation process;

Since we want to port our game to `Windows`, we must do the following:  
* Select **Windows** on the menu on the left side of the UI;
* Move to the right side of the UI and then select `Or get started now...`;
* Once there, we must select the `.stk` file we must compile from. To find it, we must hover on the `Application pack` field, and then click on the purple folder (placed in the right corner of the text field). When clicked, it should pop-up a file selection menu;
    * If you're on Windows, select the drive letter where your USB is mounted at, then pick the `Dust.stk` package by double-clicking it;
    * If you're on Linux, you must select the `Computer` icon, then select the `E:\` drive, and pick the `Dust.stk` package by double-clicking it;
* Once picked, click on the `Build` button (placed on the bottom right corner of the UI) and let the Authoring tool do it's magic;

When the whole procedure is done, you now have on your hands **Dust: Offroad Racing** unofficially ported for *Windows*!  
The port should be saved in your USB, which is exactly the place you've imported the file from, and will be outputted as `Dust_Windows.zip` by the Authoring tool.

To play the game, you need to extract the `.zip` archive, of course.  
Again, in my case i'll do it like this
```
cd /media/[username]/USB            # Here i'll get inside the USB the project has been exported to
7z x Dust_Windows.zip -oWindows     # And here i'll extract the archive, and place the files in a folder named 'Windows'
```
as a last thing, all there misses is executing the `.exe` file to play it!

Of course, on `Windows` is all a matter of getting into the folder, and then double-click on the `Dust.exe` file:  
while on `Linux` it'll be
```
wine ./Dust.exe
```
and as a final extra, if you were to ever write alongside the executable the `--help` argument, you'll find out that the Authoring tool packs a standalone version of the `ShiVa3D Engine` executable (which is yanked from `/Windows/Build/` and renamed):  
only difference is that it's configured to play the `.stk` file packaged inside (by default), but you can still use it to execute other `.stk`, `.ste`, and `.smf` packages into your machine aswell.

And with this, *this is how you can unofficially port a ShiVa-powered game to any platform you like!*

## Changing the Game Assets
This section is moreso dedicated as an introduction on changing/editing game assets from a ShiVa-powered game.  
In order to edit an asset from a ShiVa project, we should know on how to extract `.stk`, `.smf`, and `.ste` files by using **QuickBMS** (if you don't know yet how, you're free to look back on <a href="#obtaining-smf-stk-and-ste-archives">**how to obtain them**</a> and then come back here once learned).

Beyond that, in the next portions there would be practical examples on how to do the following:  
* Editing and replacing Audio assets;
* Converting, editing, and converting back textures;
* Decompile, edit, and recompile a `.lua` script into a `.lub` file;

Other types of files are pretty much excluded, since it either boils down to the format being either:  
* Proprietary and undocumented;
* May use (almost) the same steps as shown in the next sections (meaning: *converting, editing, and convert it back*);
* Or it may be plain-text data that can be easily edited with a text editor;

### Editing Audio
To edit an audio asset, we must get inside the following folder path and list it's contents
```
cd ~/Downloads/out/Resources/Sound/
ls -l
```
and from here we'll pick whatever soundfile we want to edit from- let's say we want to pick `Sound_Core_ButtonClick.snd` and replace it with a nicer sound effect, for example.

If you want to edit the file, do the following commands on your terminal  
```
cp Sound_Core_ButtonClick.snd ~/Downloads/    # Let's create a copy of the file into our Downloads folder
cd ~/Downloads                                # And then move into it
mv Sound_Core_ButtonClick.snd Sound_Core_ButtonClick.ogg    # In the end, let's change it's extension to it's real one
```

Otherwise, if you want to replace the file with a better one, this is where we'll use `ffmpeg` to convert it-  
but before conversion, though, we must know the original properties of the original file, so that the engine will handle it without issue.  
This is where `ffprobe` will come in handy for us  
```
ffprobe Sound_Core_ButtonClick.ogg
```
once we get an output from it, the audio file should have `libvorbis` as it's audio codec, and it's sample rate should be at `22050Hz`.

Once checked the latter, we'll now proceed on replacing it.  
For example purposes, the file we'll convert and use as a replacement will be called `ButtonNew.mp3`  
```
ffmpeg -i ButtonNew.mp3 -acodec libvorbis -ar 22050 Sound_Core_ButtonClick.ogg    # 'acodec''s short for 'audio codec', while 'ar' is 'audio sample rate'
```
if the tool may ask to replace the audio file in the process, type `y` in the terminal to proceed.  
When everything's done, just do the following in the terminal 
```
rm ButtonNew.mp3    # Let's remove the original copy of the new soundfile
```

And, in the end, for both methods, we must follow the commands below  
```
mv Sound_Core_ButtonClick.ogg Sound_Core_ButtonClick.snd             # Then rename the file's extension back to '.snd'
mv -f Sound_Core_ButtonClick.snd ~/Downloads/out/Resources/Sounds/   # And replace the old button SFX with our new and converted one 
```

### Editing Textures
Meanwhile, to edit a Texture asset, we must move inside  
```
cd ~/Downloads/out/Resources/Textures
ls -l
```
and, again, pick the file we want to edit- let's say we want to pick `DefaultFont.dds` and give it a different color with our image manipulation program.

Before editing the file, we must do the following before doing anything towards it  
```
cp DefaultFont.dds ~/Downloads    # Here'll be creating a copy of the file in the Downloads folder
cd ~/Downloads                      # Then move inside of it
wine ./PVRTexToolCLI -i DefaultFont.dds -o DefaultFont.png      # And convert the file into a readable format
rm DefaultFont.dds                  # And remove the original '.dds' file, just to make sure
```
and once edited the texture, we must do the following afterwards
```
wine ./PVRTexToolCLI -i DefaultFont.png -o DefaultFont.dds  # We'll convert back the texture to it's original format
rm DefaultFont.png      # Delete the edited '.png' file
mv -f DefaultFont.dds ~/Downloads/out/Resources/Textures/   # And replace the original file with our new one
```

### Editing Scripts
> ***Author's Note:***  
> *For this portion you must have installed on your machine `Lua 5.0.3` firsthand!*  
> *If you're on Windows, you can grab the compiled executables for this version [right here](https://sourceforge.net/projects/luabinaries/files/5.0.3/Tools%20Executables/):*  
> *else, if you're on Linux, you must install with the distro's provided package manager the `lua5.0` or `lua50` package.*  
> *If the latter doesn't exist into your distro's repositories, it is best advice to execute the `5.0` Windows binaries through Wine.*  
>  
> *Since ShiVa Engine uses Lua `5.0.3` for handling the game's logics, and each Lua version, with it's own built-in `luac` compiler, compiles the script with a different file signature for reflecting the version of the language used:*  
> *i would healthily suggest to stick on using that version as much as possible. Else, expect issues once it will be repackaged (like game crashes).*

And, if you want to edit a `.lua` script, we must go inside  
```
cd ~/Downloads/out/Resources/Scripts
ls -l
```
and pick whichever `.lub` script we want to edit in the first place.  

Let's say i want to edit `AI_Core_Main_CarConfigurator_Function_GetAIVar.lub`:  
in order to do so, we must do, just like the previous 2 sections, the following commands  
```
cp AI_Core_Main_CarConfigurator_Function_GetAIVar.lub ~/Downloads   # Once again, we do a copy of the file in 'Downloads'
cd ~/Downloads                                                      # Then move inside of it
java -jar unluac.jar -i AI_Core_Main_CarConfigurator_Function_GetAIVar.lub -o AI_Core_Main_CarConfigurator_Function_GetAIVar.lua    # And convert the script into '.lua'
```
and once edited the script, we must do  
```
rm *.lub    # Remove every '.lub' file inside the current folder
wine ./luac50.exe -o AI_Core_Main_CarConfigurator_Function_GetAIVar.lub AI_Core_Main_CarConfigurator_Function_GetAIVar.lua  # Compile back the '.lua' script into '.lub'
rm AI_Core_Main_CarConfigurator_Function_GetAIVar.lua   # Remove the edited version of the script
mv -f AI_Core_Main_CarConfigurator_Function_GetAIVar.lub ~/Downloads/out/Resources/Scripts/     # And replace the original with our new and compiled '.lub' script
```

## Repacking and Testing
> ***Author's Note:***  
> *Sadly, this process only works if you're on PC.*  
> *Since for other platforms it might either imply recompiling the game from scratch or having a more convoluted way to make everything work:*  
> *this portion will only show on how to do the latter for Windows & Linux.*  

And to finish everything in beauty, we'll now learn on how to repackage our edited `.stk`/`.smf` file:  
all by using **QuickBMS**, *some hacky way to make our mod work*, and **ShiVa Authoring Tool**!

The very first thing that we must do is deciding which file/s we want to reimport to our previously extracted `S3DMain.smf`.  
Let's say that we have edited a texture file (as shown previously on <a href="#editing-textures">**how to edit and replace one**</a>) and we want to import it into our game and see the results.

As the [official documentation](https://aluigi.altervista.org/papers/quickbms.txt) for **QuickBMS** mentions:  
if we want to make the reimporting much easier for the program, we must get rid of the unedited assets from the extracted `.smf` archive.  
To make the whole thing quicker, just follow through these commands i had written below  
```
cd ~/Downloads/out                  # We'll move where we had previously extracted the file from
rm -rf Games Models Scenes          # To start with, we'll remove the folders outside of the 'Resources' folder
cd Resources                        # Get into the 'Resources' folder
rm -rf `ls | grep -v Textures`      # Remove every folder that isn't the 'Textures' one
cd Textures                         # Move into the 'Textures' folder
rm `ls | grep -v DefaultFont.dds`   # And remove every file outside of the one we've edited
cd ~/Downloads/BMS/                 # In the end, we'll move back to the QuickBMS folder
```

With those removed, now it is our time to reimport the edited assets back to the file.  
If the file is *smaller* or *the same size as the original file*, write this command
```
./quickbms -w -r shiva.bms ~/Downloads/assets/S3DMain.smf ~/Downloads/out/
```
if you're wondering what's the `-w` argument for, that gives the tool permission to write on the selected archive

Else, if the edited file is ***much bigger*** than the original, write this command instead
```
./quickbms -w -r -r shiva.bms ~/Downloads/assets/S3DMain.smf ~/Downloads/out/
```
the double `-r`s, yet according to the official documentation, enables a mode called `REIMPORT2`.  
With this mode enabled, `QuickBMS` will reimport any asset bigger than the original without any issues or errors to speak of.

If using the terminal ain't your strong suit, it's documentation introduces other ways that would involve using the GUI:  
such as dropping the edited files towards it's `reimport` batch files, but i'll leave the explanation to the wonderful docs written by `ALuigi`.

After the whole reimport is done, you may wonder: *what has happened to the game package?*  
Into very simple words, `QuickBMS` has unintentionally broke the `.stk`/`.smf` file- but the reimporting process wasn't the cause!  

The real cause is that ShiVa's `S3DEngine` executable has a so-called **CRC check** feature compiled inside.  
This *very particular* check, in a few words, compares the `CRC` offsets (which uses the `0x11112222` syntax) of the former file's and the new one's (that we had reimported previously):  
* *If the file* ***does*** *pass the check*, it gets loaded into the game as-is;
* *If the file* ***doesn't*** *pass the check*, the engine won't load the "compromised" file, it'll log the issue in a logfile (called `S3DClient.log`), and it would try to find for locally-stored replacement files inside the host machine;

Also, part of the issue is that `QuickBMS` doesn't support `CRC` at all (which is, too, mentioned in it's docs), this causing the package to "break" when we reimport a modified asset:  
though, because of this, it actually unlocked for us an *exploit* we could take advantage of.

To make our edited asset load successfully (and ignore the `CRC` check), we must copy the whole folder path whence it came from and paste it into a specific folder path the engine references from.  
On Linux, we can do the following  
```
cd ~/Downloads/out/     # Let's get into the path where our edited asset is at
cp -r * ~/.StoneTrip/   # And then recursively copy it's contents into the engine's folder 
```

On the other hand, on Windows, you must paste the whole thing into the following folder path  
```
C:\users\[username]\AppData\Roaming\Stonetrip\Player\[XXXXXXXX]\
```
if you're wondering what's `[XXXXXXXX]`, it's actually a placeholder i've put in there.  
When a ShiVa-powered game is booted under Windows, it creates not only the `Stonetrip` folder, but also one for the game aswell:  
which it gets assigned an `8-digit` hexadecimal ID for internal use. For example, **Dust:Offroad Racing**'s internal ID is `4ee121e9`.

And when the whole copying is done, we must now test if our mod will work:  
this by recompiling the repacked `S3DMain.smf` file with **ShiVa Authoring Tool**, picking the target platform, and use the standalone `S3DEngine` executable (that'll be included inside the outputted `.zip` archive) to see if it works.  
I'll save myself the whole explanation, since the process is the same as <a href="#creating-unofficial-ports">**creating unofficial ports**</a> for your favorite ShiVa-powered game.  

Once compiled the game, you probably don't need to it ever again, unless you're aiming to mod a different ShiVa-powered game.  

Reason why recompilation isn't necessary is due to this:  
you can actually get inside the folder path the engine refers to, and then edit the file without having to do any reimports ever again- but that's proportioned between the amount of reimported assets into the `.smf`/`.stk` archive and the locally-stored file/s inside the `Stonetrip` folder.  
If you had reimported 1 asset, then you can edit just 1. If it's 2, then you can edit 2, etc.  
You can rinse and repeat the whole process until you're satisfied with the results.

And that's it: *you've officially learned on how to create a mod for your favorite game made in ShiVa!*

### Distributing your Mod
Now that you've saw on how to *make* a mod, how can we *distribute* it?

It's actually simple, we just need to take the `.zip` we have obtained from the *Authoring Tool* and add inside every file coming from the `Stonetrip/Resources/` folder path!

If you're on Linux, do the following  
```
cd /media/[username]/USB                        # Let's get into the USB to find the 'Dust_Linux.zip' archive
7z a Dust_Linux.zip ~/.StoneTrip/Resources/*    # Use 7zip to include the whole contents of 'Stonetrip/Resources/' inside of it
```

If you're Windows, do these steps:  
* Get inside of your USB via your file explorer;
* Find the `Dust_Windows.zip` archive;
* Right click on it and click on `7-Zip`>`Add to "Dust_Windows.zip"`;
* When `7-zip` opens, copy `C:\users\[username]\AppData\Roaming\Stonetrip\Player\4ee121e9\` and paste it inside the path field;
    * Make sure to replace `[username]` with your own username;
* Select the `Resources` folder;
* Click on the `Add` button;
* Let the program do it's magic!

If you feel eager to add a `readme` file for instructions on how to play and install the mod:  
you can add it to the archive by repeating the whole process above.

And when the whole thing is done, *you can now distribute your mod wherever you like!*

---

# Epilogue 
And with all of this, here comes to an end to this exhaustive documentation i've dedicated myself to write about this dead, yet nichè, but fascinatingly surprising game engine that is **ShiVa Engine**.  
I never knew that this game engine relied a lot to a great chunk of FOSS tools, but i am so glad that the devs really didn't hide a lot which these were in the first place, too.

I hope that whoever read all of this rich documentation has enjoyed the ride as i did with researches, tinkering, experimenting, self-improvement, and the countless years of discoveries it had took me to figure out everything about ShiVa:  
but now? all of this information is not anymore hidden to everyone on the internet, you can all thank me about it lmao.

---

# Sources
* [**Wayback Machine**](https://archive.org), for being incredibly useful for both my research on the company & the engine itself, but also was wonderful for having the needed tools for this documentation archived and accessible for everyone;
* **XenTax** and **ZenHax**<sup>*rip*</sup>, formerly had very relevant info on the engine itself, such as `.mdo` files containing plain-text data, but also information about `.smf` and `.stk` files being just proprietary archives;
* [**DLL Files.com**](https://dll-files.com), which i had used as a reference for any `.dll` file contained within the engine; 
* [**Online TrID**](https://mark0.net/onlinetrid.html) by *Marco Pontello*, which it introduced me years ago to the world of *file signatures*, but also used it along my years of research on figuring certain proprietary formats along the way;
* [**OpenJDK.org**](https://openjdk.org) & [**Microsoft Learn**](https://learn.microsoft.com/en-us/), builds and documentation for `OpenJDK`;
* [**i3DConverter**](http://3doc.i3dconverter.com/index.html), the *only* tool on the internet that gave me the hint that `.msh` files contain actual raw model data;
* [**Imagination Technology's documentation**](https://docs.imgtec.com), documentation for *PVRTexTool*;
* [**3D Game Development with ShiVa3D Suite: AI & Animation**](https://code.tutsplus.com/3d-game-development-with-shiva3d-suite-ai-animation--mobile-9905a), source used for documenting the usage for `.ste` and `.stk` archives;
* [**Android Developers**](https://developer.android.com), documentation on how to enable *Developer Options* and *USB Debugging*;
* [**Lua.org**](https://www.lua.org/), manpage for the `5.0` edition of the `luac` compiler;
* [**LuaBinaries**](https://sourceforge.net/projects/luabinaries/), a distribution of `Lua` that offers executables for various platforms;
* [**QuickBMS**](https://aluigi.altervista.org/quickbms.htm), documentation for *QuickBMS*;
* **Myself**, which i had messed up with the engine's components, throughout the years, in order to figure it's ins-and-outs;
